FROM debian:bullseye-slim

LABEL maintainer="kali <kali@leap.se>"

RUN apt-get update \
    && apt-get install --no-install-recommends -y  \
    curl ca-certificates make jq \
    && rm -rf /var/lib/apt/lists/*
