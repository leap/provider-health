#!/usr/bin/env python3
#
# This script generates an input json file for `service-prober`.
# It only collects openvpn endpoints for now.
#
from collections import OrderedDict
import json

import requests

EIP_SERVICE = "https://api.black.riseup.net/3/config/eip-service.json"
API_CA = "./ca.crt"

remotes = []

def collectEndpointInfo(gateways):
    for gw in gateways:
        ip = gw['ip_address']
        cap = gw['capabilities']
        transports = cap['transport']
        for tr in transports:
            if tr['type'] != "openvpn":
                continue
            ports = tr['ports']
            protocols = tr['protocols']
            for port in ports:
                for proto in protocols:
                    remotes.append({
                        "ip": ip,
                        "proto": proto,
                        "port": port
                        })

openvpn_probe = OrderedDict({
        "type": "openvpn_ping",
        "name": "openvpn_ping/${remotes.ip}:${remotes.port}/${remotes.proto}",
        "loop": ["remotes"],
        "params": {
          "pingTarget": "1.1.1.1",
          "remote": "${remotes.ip}",
          "proto": "${remotes.proto}",
          "port": "${remotes.port}",
          "cipher": "AES-256-GCM",
          "auth": "SHA1",
          "cert": "/tmp/cert.pem",
          "key": "/tmp/cert.pem",
          "ca": "/tmp/ca.crt"
      }
})

if __name__ == "__main__":
    r = requests.get(EIP_SERVICE, verify=API_CA).json()

    collectEndpointInfo(r['gateways'])

    probe = OrderedDict({})
    probe['vars'] = {}
    probe['vars']['remotes'] = remotes
    probe['probes'] = [ openvpn_probe ]

    print(json.dumps(probe))

