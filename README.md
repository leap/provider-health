# Provider health

An ad-hoc pipeline to have a reference of when different critical components of the infrastructure are down.

This can probably be superseded by lilypad infra.

## Generate service probe

```
make gen_service_spec > openvpn-probe.json
```
