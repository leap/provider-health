LOCATION ?= miami
RISEUP_CA=https://black.riseup.net/ca.crt
RISEUP_CA_TMP=/tmp/riseup-ca.crt
CALYX_CA=https://calyx.net/ca.crt
EIP=https://api.black.riseup.net/3/config/eip-service.json 

check_riseup:
	curl ${RISEUP_CA}  > ${RISEUP_CA_TMP}
	curl --cacert ${RISEUP_CA_TMP} https://api.black.riseup.net:9001/json
	curl --cacert ${RISEUP_CA_TMP} ${EIP} > /tmp/riseup-eip.json

check_calyx:
	curl ${CALYX_CA}

get_openvpn_gateways:
	curl -k ${RISEUP_CA}  > ${RISEUP_CA_TMP}
	curl --cacert ${RISEUP_CA_TMP} ${EIP} | jq '.gateways|map(.ip_address, .location)'

get_obfs4_bridges:
	curl -k ${RISEUP_CA}  > ${RISEUP_CA_TMP}
	curl --cacert ${RISEUP_CA_TMP} ${EIP} | jq '.gateways[] | select(.capabilities.transport[] | .type == "obfs4") | {"host": .host, "ip": .ip_address, "port": 23042}'

list_by_location:
	LOCATION=${LOCATION} curl --cacert ${RISEUP_CA_TMP} ${EIP} | jq '.gateways[] | select(.location == env.LOCATION) | {"location": .location, "host": .host, "ip": .ip_address}'

gen_service_spec: 
	@/usr/bin/curl --cacert ./scripts/ca.crt	 https://api.black.riseup.net/3/cert > /tmp/cert.pem
	@cp ./scripts/ca.crt /tmp/ca.crt
	@cd scripts && python3 gen-service-probes.py

build:
	@echo "nothing to do"

